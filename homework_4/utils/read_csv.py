import csv

KG_POUNDS = 2.20462
SM_INCHES = 0.393701


def get_csv_data():
    all_weight = 0
    all_height = 0
    total_number = 0
    file_path = "assets/hw.csv"

    with open(file_path, "r") as f:
        reader = csv.reader(f)
        for row in reader:
            if not row:
                continue

            number = row[0]
            height = row[1]
            weight = row[2]

            if not number.isdigit():
                continue
            total_number = int(number)
            all_height += float(height)
            all_weight += float(weight)

    average_height = height_in_sm(all_height / total_number)
    average_weight = weight_in_kg(all_weight / total_number)
    return {'height': average_height, 'weight': average_weight}


def weight_in_kg(weight_in_pounds):
    weight = round(weight_in_pounds / KG_POUNDS, 2)
    return weight


def height_in_sm(height_in_inches):
    height = round(height_in_inches / SM_INCHES, 2)
    return height
