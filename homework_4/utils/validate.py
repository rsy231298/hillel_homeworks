param_ranges = {
    'length': {'start': 1, 'end': 100},
    'specials': {'start': 0, 'end': 1},
    'digits': {'start': 0, 'end': 1},
}


def validate(request, request_param):
    if request_param not in request or not request[request_param].isdigit():
        return False

    value = int(request[request_param])

    if request_param == 'length':
        if in_range(request_param, value):
            return value
    elif request_param == 'specials' or request_param == 'digits':
        if in_range(request_param, value):
            return True if value == 1 else False
    return False


def in_range(param, value):
    param_range = param_ranges[param]
    if param_range['start'] <= value <= param_range['end']:
        return True
    return False
