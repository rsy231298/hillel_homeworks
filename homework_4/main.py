import random
import string
from homework_4.utils.read_csv import get_csv_data
from homework_4.utils.read_txt import get_txt_data
from homework_4.utils.validate import validate


from flask import Flask, request
app = Flask(__name__)


@app.route('/avr_data')
def get_avr_data():
    res = get_csv_data()
    return f"Average height {res['height']} and average weight {res['weight']}."


@app.route('/requirements')
def get_requirements():
    res = get_txt_data()
    return res.replace('\n', '<br>')


@app.route('/random')
def get_random():
    length = validate(request.args, 'length')
    if not length:
        return 'Length must be great then 0 and not great then 100.'

    specials = validate(request.args, 'specials')
    digits = validate(request.args, 'digits')
    result = ''
    for i in range(length):
        char_type = random.randint(1, 3)
        if char_type == 1:
            result += random.choice(string.ascii_lowercase)
        elif char_type == 2 and digits:
            result += random.choice(string.digits)
        elif char_type == 3 and specials:
            result += random.choice(string.punctuation)
        else:
            result += random.choice(string.ascii_lowercase)
    return result


if __name__ == '__main__':
    app.run(debug=True)

